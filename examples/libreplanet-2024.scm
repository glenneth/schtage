(slides
 (title "Cultivating a Welcoming Free Software Community That Lasts"
        #:author "David Wilson"
        #:organization "System Crafters"
        #:url '(a (@ (href "https://systemcrafters.net")) "systemcrafters.net")
        #:event "LibrePlanet 2024")

 "1. why community matters"
 "2. cultivating community"
 "3. igniting collaboration"
 "4. the long term"

 (section "part 1"
          "why community matters 💡")

 "free software movement"
 "40 YEARS\n😱"
 "GNU" ;; use the logo?

 "the mission"
 "promote\nuser freedom"
 "a global community"

 "not (just) about\nsoftware"
 "it's about\npeople"

 "community matters."

 "more\npeople"
 "more\nwelcoming"

 "freedom\nfor all"

 (section "part 2"
          "cultivating community 🌳")

 (list "what is welcoming?"
       "friendly"
       "helpful"
       "non-judgemental")

 (bullets "cool"
          "fun"
          "yes")

 (section "part 3"
          "igniting collaboration 🔥")

 (section "part 4"
          "the long term 🌅")

 "go forth\nand welcome everyone!")
